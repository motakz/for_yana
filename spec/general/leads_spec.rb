require 'spec_helper'

describe "Create a new lead and check it" do

  before(:all) do
    login_to_autotest
    visit(LeadsPage)
  end

  it "Create a new lead" do
    on(LeadsPage).proceed_to_add_new_lead
    on(LeadsNewPage).fill_in_lead_details
    on(LeadsNewPage).save_lead
    on(LeadDetailsPage).is_lead_created?
  end
  
  it "Add a new note for the lead" do |note_content|
    on(LeadDetailsPage).add_new_note
  end
  
  it "Check if note is visible" do
    on(LeadDetailsPage).is_note_visible?
  end

end