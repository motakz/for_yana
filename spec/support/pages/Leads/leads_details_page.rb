class LeadDetailsPage < AbstractPage
	include PageObject
	include RelatedToPicker

	textarea(:note_body, :class => "span12", :name => "note")
	button(:save_button, :css => ".btn.btn-inverse.hide")
	unordered_list(:notes_list, :css => ".feed-items")
	span(:lead_title, :css => ".detail-title")

	def is_lead_created?
		lead_title_element.wait_until_present
		text.include?("Yana Lead").should == true
	end
	
	def add_new_note
		note_body_element.when_visible(5).click
		note_body_element.set "Lead_Note"
		save_button_element.click
	end

	def is_note_visible?
		text.include?("Lead_Note").should == true
	end

end