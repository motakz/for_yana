class LeadsNewPage < AbstractPage
  include PageObject

  page_url "https://app.futuresimple.com/leads/new"
  
  text_field(:lead_firstname, :id => "lead-first-name")
  text_field(:lead_lastname, :id => "lead-last-name")
  text_field(:lead_company, :id => "lead-company-name")
  button(:save_lead, :css => '.save.btn')

  def fill_in_lead_details
  	    lead_firstname_element.when_visible(10).click
  	    lead_firstname_element.set "Yana"
  	    lead_lastname_element.set "Lead"
  end

  def save_lead
  	    save_lead_element.click
  end
  
end