class LeadsPage < AbstractPage
	include PageObject

	page_url "https://app.futuresimple.com/leads"

	button(:add_new_lead, :css => "#leads-new")
	div(:leads_list, :class => "objects")
	list_item(:empty_info, :class => "item empty")
	text_area(:note_body, :class => "span12", :name => "note")
	button(:save, :class => "btn btn-inverse hide", :text => "Save")

	def proceed_to_add_new_lead
		add_new_lead_element.when_visible(10).click
	end
	
end